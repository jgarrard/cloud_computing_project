#!./py2env/bin/python2.7
import gensim
import sys
import time
import threading
import botocore
import boto3
import json

from dependencies import preprocess

def compare(doc, dic, model):
    vector = dic.doc2bow(preprocess(doc))
    resp = []
    for index, score in sorted(model[vector], key=lambda tup: -1*tup[1]):
        resp.append([index,score])
    return resp

if(len(sys.argv) != 3):
    print("Improper use\n./loc_tweetClassify <tweetFileName> <easy/hard>")
    exit()

if(sys.argv[2] == "easy"):
    dct_path = '../model/easy_dic.txt'
    mod_path = '../model/easy_model.lda'
elif (sys.argv[2] == "hard"):
    dct_path = '../model/hard_dic.txt'
    mod_path = '../model/hard_model.lda'
else:
    print("Did not recognize model difficulty")
    exit()

t0 = time.time()

dct = gensim.corpora.Dictionary.load_from_text(dct_path)
mod = gensim.models.LdaModel.load(mod_path)

f = open(sys.argv[1], "r")
text = f.read().rstrip().split("\n")

threads=[]
results=[]
for line in text:
    result = compare(line, dct, mod)
    results.append(result)


f.close()
t1 = time.time()

print("Tweets processed: {}".format(len(results)))
print("Time: {}".format(t1-t0))


