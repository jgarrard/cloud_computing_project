#!./py2env/bin/python2.7

import gensim       #LDA stuff
import sys          #sys.argv
import time         #timing
import threading    #lambda thread
import botocore     #configs of aws resources
import boto3        #aws resources
import json         #json.loads()
import math         #ceil

def lambda_worker(data):
    boto_config = botocore.config.Config(read_timeout=315)
    client = boto3.client('lambda', config =boto_config)

    payload = {'data':data}
    response = client.invoke(
        FunctionName="tweetModel",
        InvocationType="Event",
        Payload=json.dumps(payload)
    )
    #wonder what gets returned here?
    #print(response)
    #it's just a successful submission message

def thread_lambda_worker(data):
    boto_config = botocore.config.Config(read_timeout=315)
    client = boto3.client('lambda', config =boto_config)

    payload = {'data':data}
    response = client.invoke(
        FunctionName="tweetModel",
        InvocationType="RequestResponse",
        Payload=json.dumps(payload)
    )
    resp = json.loads(response['Payload'].read())
    if('response' in resp.keys()):
        print("Number of results from last Lambda: {}".format(len(resp['response'])))
    else:
        print(resp)


if(len(sys.argv) != 4):
    print("Improper use!\n./tweetClassify.py <inputFile> <numberTweets> <lambdaBatchSize>")
    exit()

#get batch size and twitter text information
batchSize = int(sys.argv[3])

#Ideally, tweets doesn't need to be known at runtime, it would be a "stream", but for testing I need to deploy in known batch sizes, so for now the user needs to supply
tweets = int(sys.argv[2])

#tweets should never be greater than batchSize*1000->Lambda Concurrency Limit
if(tweets > batchSize*1000):
    print("The number of tweets at the given batch size will exceeded the Lambda Concurrency Load")
    exit()

f = open(sys.argv[1], "r")

print("NumTweets: {}".format(tweets))

numLambdas = int(math.ceil(float(tweets)/batchSize))
print("Lambdas to Deploy: {}".format(numLambdas))

#setup aws db resource
dynamoDB = boto3.resource('dynamodb')
table = dynamoDB.Table('tweets')

#admin for reading tweets from file, need users, tweets, and threads
threads=[]
tweets=[]
resp = []
currUser = ""
userC = 0
batchC = 0

t0 = time.time()
for line in f:
    uname, tweet = line.split("*")  
    if(uname != currUser):
        currUser = uname
        userC = 0
    userC+=1
    tweets.append([uname,userC,tweet])
    
    if(len(tweets) == batchSize):
        batchC+=1
        if(batchC == numLambdas):
            #Launch a thread for the last lambda batch
            print("Launching thread for last batch")
            t = threading.Thread(target=thread_lambda_worker, args=(tweets,))
            threads.append(t)
            t.start()
            tweets = []
        else:
            #print("Submitting batch {}".format(batchC))
            #Launch async lambda -> only see results in db
            lambda_worker(tweets)
            tweets = []

f.close()
#Need to send last batch if numTweets%batchsize!=0
if(len(tweets) != 0):
    print("Leftovers: launching thread for last batch")
    #Need to thread!
    resp = thread_lambda_worker(tweets)
    tweets = []

#This should only ever be one thread, but putting in for loop just for safe execution
for t in threads:
    t.join()

#Once the last thread finishes, we can start scanning the DB table until 1000 (every lambda) have been published
scanned = table.scan()
scanned = scanned['ScannedCount']
prev_scanned = scanned

while(scanned != numLambdas):
    scanned = table.scan()
    if('ScannedCount' in scanned.keys()):
        scanned = scanned['ScannedCount']
    else:
        print("DB could not be scanned")
        continue
    if(prev_scanned != scanned):
        print("New total in DB: {}".format(scanned))
        prev_scanned = scanned

#scanning timeout? 

#Stop timing once scanned == numLambdas

t1 = time.time()

print("Time: {}".format(t1-t0))


