import string

#TODO

#def lemmatize_stemming(text):
#    return []

def preprocess(text):
    f = open('./english_stopwords', 'r')
    STOPWORDS = f.read()
    f.close()

    letters = string.ascii_letters
    result = []
    for word in text.split(" "):
        word = word.lower()
        if word not in STOPWORDS and len(word) > 2:
            f_word = ''
            for char in word:
                if char in letters:
                    f_word = f_word + char
            if(len(f_word) > 0):
                result.append(f_word) 
    return result

def countWords(text):
    c = 0;
    for s in text:
        c+=len(s)
    return c



