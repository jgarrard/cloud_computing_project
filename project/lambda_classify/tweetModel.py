import gensim
import boto3.dynamodb.types
import boto3
from dependencies import preprocess

def lambda_handler(event, context):
    dct = gensim.corpora.Dictionary.load_from_text('./dic.txt')
    mod = gensim.models.LdaModel.load('./model.lda')
   
    dynamoDB = boto3.resource('dynamodb')
    table = dynamoDB.Table('tweets')
 
    resp = []
    #tweet[0] = username
    #tweet[1] = tweetNumber for given user
    #tweet[2] = tweet
    for tweet in event['data']:
        vector = dct.doc2bow(preprocess(tweet[2]))
        topCat = sorted(mod[vector], key=lambda tup: -1*tup[1])[0]
        index = topCat[0]
        score = boto3.dynamodb.types.Decimal(float(topCat[1]))
        resp.append([tweet[0], str(tweet[1]), index, score])    #tweetNum needs to be cast to str for DB 
    
    #For testing, just writing first tweetNumber and index of last tweet to DB
    #In production, would write every tweet keyed on username and tweetNumber
    last = resp[len(resp)-1][1] #index number of last processed tweet
    table.put_item(
        Item={
            'username':resp[0][0],
            'tweetNum':resp[0][1],
            'lastTweet':last
        }
    )
    msg = ""
    status = 200
    
    return {'statusCode':status, 'message':msg, 'response':resp}


