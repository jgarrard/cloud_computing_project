#!./py3env/bin/python3

#For testing the form of reddit json requests and responses

import requests
import os
import gensim
import nltk
import time

from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer

stemmer = PorterStemmer()
limit = 50

#TODO

def lemmatize_stemming(text):
    return stemmer.stem(WordNetLemmatizer().lemmatize(text, pos='v'))

def preprocess(text):
    result = []
    for token in gensim.utils.simple_preprocess(text):
        if token not in gensim.parsing.preprocessing.STOPWORDS and len(token) > 2:
            result.append(lemmatize_stemming(token))
    return result

def countWords(text):
    c = 0;
    for s in text:
        c+=len(s)
    return c

def scrape(event):
    data = []
    msg = ""
    status = 200
    sub = event['sub']

    #make request
    addy = 'https://www.reddit.com/r/{}/.json'.format(sub)
    headers  = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}
    response = requests.get(addy, headers=headers)

    if(response.status_code == 404):
        #Catch requests error
        msg = "The {} page was not found :(".format(sub)
        status = 400

    links = []
    count = 0
    
    t0 = time.time()
    while(count < int(limit) and status != 400):
      response = response.json()
      if(not response):
        #Catch response but no data error
        msg = "No response received"
        status = 400
        break

      response = response['data']
      after = response['after']
      response = response['children']

      for element in response:
        element = element['data']
        links.append(element['permalink'])
        data.append(element['title'])
        count = count + 1

      response = requests.get(addy, headers=headers, params={'after':after})


    if(status != 400):
      for link in links[1:]:
        commentAddy = 'https://www.reddit.com{}/.json'.format(link)
        response = requests.get(commentAddy, headers=headers)
        response = response.json()
        response = response[1]
        response = response['data']
        response = response['children']
        for child in response:
          if 'body' in child['data'].keys():
            data.append(child['data']['body'])         
    
    t1=time.time()
    if(len(data)>0):
        result = [preprocess(x) for x in data]
    t2=time.time()

    return {'statusCode':200, 'Message':msg, 'Data':result, 'Processed':countWords(data), 'Times':[t1-t0,t2-t1]}



