#!./py3env/bin/python3

import gensim       #modeling
import boto3        #aws lambda
import botocore     #settings for lambda
import threading    #threaded lambda calls
import json         #encoding/decoding
import requests     #requests to reddit
import os           #os.environ['USER']
import time         #timing
import sys          #sys.argv
from dependencies import preprocess     #cleaning text data
from dependencies import countWords     #counting words in a set of text

def lambda_worker(text):
    global words
   
    #set timeout and create lambda client 
    boto_config = botocore.config.Config(read_timeout=315)
    client = boto3.client('lambda', config=boto_config)
    
    #submit requests response text to lambda for text cleaning
    data = {'text': text}
    response = client.invoke(
        FunctionName="lambdaCleanup",
        InvocationType="RequestResponse",
        Payload=json.dumps(data)
    )

    resp = json.loads(response['Payload'].read())
    data2 = resp['Data']
    
    #add cleaned text to global words list
    w_lock.acquire()
    words = words + data2
    w_lock.release() 

if(len(sys.argv) != 2):
    print("Improper use\n./model.py <easy/hard>")
    exit()

#list of reddit pages to scrape
if(sys.argv[1] == "hard"):
    #yields more complicated model
    subs = ['politics','linux','gaming','BikiniBottomTwitter','Marvel','StarWars','personalfinance','news','college']
elif(sys.argv[1] == "easy"):
    #yields simpler model
    subs = ['politics','linux']
else:
    print("Unrecognized model difficulty")
    exit()

#threads will deposit there cleaned text data to this list of words
words = []
w_lock = threading.Lock()

print("Scraping: {}".format(subs))
threads = []
for sub in subs:
    #each sub will only fetch 250 articles 
    limit = 20
    data = []
  
    #begin timing requests 
    t0 = time.time() 
    #make request
    addy = 'https://www.reddit.com/r/{}/.json'.format(sub)
    headers  = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}
    response = requests.get(addy, headers=headers)

    if(response.status_code == 404):
        print("Failed to reach {} page".format(sub))
        continue 

    #links will contain the urls to the comments sections
    links = []
    count = 0

    while(count < int(limit)):
      response = response.json()
      if(not response):
        #Catch response but no data error
        print("Empty response from {} page".format(sub))
        break

      response = response['data']
      after = response['after']
      response = response['children']

      for element in response:
        element = element['data']
        links.append(element['permalink'])
        data.append(element['title'])
        count = count + 1
        
      #continue making requests from the last article returned (each requests only ships back ~50 articles)
      response = requests.get(addy, headers=headers, params={'after':after})

    #process comment sections
    for link in links[1:]:
        commentAddy = 'https://www.reddit.com{}/.json'.format(link)
        response = requests.get(commentAddy, headers=headers)
        response = response.json()
        response = response[1]
        response = response['data']
        response = response['children']
        for child in response:
          if 'body' in child['data'].keys():
            data.append(child['data']['body'])
    t1 = time.time()

    print("Fetched {}'s {} words in {}s".format(sub, countWords(data), t1-t0))
    t = threading.Thread(target=lambda_worker, args=(data,))
    threads.append(t)
    t.start()

#wait for threads to finish
main_thread = threading.currentThread()
for t in threads:
    if t is main_thread:
        continue
    t.join()

print("Total words after cleaning: {}".format(countWords(words)))

dic = gensim.corpora.Dictionary(words)
print("Dictionary size: {}".format(dic.num_pos))

corpus = [dic.doc2bow(doc) for doc in words]

#time model generation
t0 = time.time()
ldaModel = gensim.models.ldamodel.LdaModel(corpus, num_topics = len(subs), id2word=dic, passes=12)
t1 = time.time()

#save model and dictionary results to models folder
#needed for local classify tests and for lambda classifier
if(len(subs) > 2):
    ldaModel.save('../model/hard_model.lda')
    dic.save_as_text('../model/hard_dic.txt')
else:
    ldaModel.save('../model/easy_model.lda')
    dic.save_as_text('../model/easy_dic.txt')
   
print("Modeling time: {}\nTopics:{}".format(t1-t0, ldaModel.print_topics()))

