import os
import time

from dependencies import preprocess
from dependencies import countWords


def lambda_handler(event, context):
    msg = ""
    status = 200
    data = event['text']

    t1=time.time()
    result = [preprocess(x) for x in data]
    t2=time.time()

    return {'statusCode':200, 'Message':msg, 'Data':result, 'Processed':countWords(data), 'Times':t2-t1}




